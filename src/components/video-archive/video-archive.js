import React, { Component } from "react";

import bannerImage from "./../../images/banner-snap.png";
import "./video-archive.css";
import data from "./sessions.json";

import TrackNav from "./../track-nav/track-nav";
import ArticleList from "../article-list/article-list";
import Article from "../article/article";

class VideoArchive extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tracks: [],
      activeTrack: "",
      activeArticle: null
    };
  }

  handleTrackClick = track => {
    this.setState({
      activeTrack: track,
      activeArticle: null,
    });
  };
  
  handleArticleClick = article => {
    this.setState({
      activeArticle: article
    })
  }

  getArticlesByTrack = (activeTrack) => {
    let articles = [];
    data.Items.forEach(item => {
      if (!item.Track) {
        return;
      }
      if (item.Track.Title === activeTrack) {
        articles.push(item);
      }
    });
    return articles;
  };

  getTracks = () => {
    let tracks = [];
    data.Items.forEach(item => {
      if (!item.Track) {
        return;
      }
      if (!tracks.includes(item.Track.Title)) {
        tracks.push(item.Track.Title);
      }
    });
    return tracks;
  };

  render() {

    let tracks = this.getTracks();
    let activeTrack = this.state.activeTrack || tracks[0];

    let articles = this.getArticlesByTrack(activeTrack);
    let activeArticle = this.state.activeArticle || articles[0];

    return (
      <div>
        <img src={bannerImage} alt="banner" className="atl-banner" />
        <TrackNav
          tracks={tracks}
          activeTrack={activeTrack}
          handleTrackClick={this.handleTrackClick}
        />
        <div className="container">
          <div className="row">
            <div className="col-4">
              <ArticleList
                articles={articles}
                track={activeTrack}
                activeArticle={activeArticle}
                handleArticleClick={this.handleArticleClick}
              />
            </div>
            <div className="col-8">
              <Article article={activeArticle} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default VideoArchive;
