import React, { Component } from "react";
import "./article-list.css";

import { getSpeakers } from "./../helpers";

class ArticleList extends Component {
  render() {
    return (
      <aside>
        <h3 className="heading">{this.props.track}</h3>
        <ul>
          {this.props.articles.map((article, i) => {
            return (
              <li
                key={`article-${i}`}
                className={
                  this.props.activeArticle.Id === article.Id
                    ? "atl-article-list-item active"
                    : "atl-article-list-item"
                }
                onClick={() => this.props.handleArticleClick(article)}
              >
                <h6 className="title">{article.Title}</h6>
                {getSpeakers(article.Speakers)}
              </li>
            );
          })}
        </ul>
      </aside>
    );
  }
}

export default ArticleList;
