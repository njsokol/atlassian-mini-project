import React, { Component } from "react";
import "./article.css";

import { getSpeakers } from "./../helpers";

class Article extends Component {
  render() {
    return (
      <article className="atl-article">
        <h1 className="heading">{this.props.article.Title}</h1>
        {getSpeakers(this.props.article.Speakers)}
        <br />
        <p>{this.props.article.Description}</p>
      </article>
    );
  }
}

export default Article;
