import React, { Component } from "react";
import "./track-nav.css";

class TrackNav extends Component {

  render() {
    return (
      <nav>
        <div className="container">
          <ul className="atl-nav-tabs">
            {this.props.tracks.map(track => {
              return (
                <li
                  key={track}
                  className={this.props.activeTrack === track ? "active" : ""}
                  onClick={() => this.props.handleTrackClick(track)}
                >
                  {track}
                </li>
              );
            })}
          </ul>
        </div>
      </nav>
    );
  }
}

export default TrackNav;
