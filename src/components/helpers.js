import React from 'react';

/**
 * Return the speakers in a readable format
 * @param {array} speakers
 */
export function getSpeakers(speakers) {
  // Are there any speakers for this article
  if (!speakers) {
    return "";
  }
  // Cool, return the speakers in a readable format
  // FirstName LastName - Company
  return speakers.map((speaker, idx) => {
    return (
      <span className="speaker" key={`speaker-${idx}`}>
        {`${speaker.FirstName} ${speaker.LastName} - ${speaker.Company}`}
        <br />
      </span>
    );
  });
}
