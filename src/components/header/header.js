import React, { Component } from "react";
import brandImage from "./../../images/header-snap.png";
import "./header.css";

class Header extends Component {
  render() {
    return (
      <header className="atl-header">
        <div className="container">
          <img
            src={brandImage}
            alt="atlassian-summit-brand"
            className="brand"
          />
        </div>
      </header>
    );
  }
}

export default Header;
