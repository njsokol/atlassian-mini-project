import React from "react";
import ReactDOM from "react-dom";
import { unregister } from "./registerServiceWorker";

import './reset.css'; // Reset styling before we begin
import './type-scale.css'; // Use https://type-scale.com/ for a good starting point
import './index.css'; // Global styles for the app

import Header from './components/header/header';
import VideoArchive from './components/video-archive/video-archive';

ReactDOM.render(
  <main>
    <Header />
    <VideoArchive />
  </main>,
  document.getElementById("root")
);

unregister();
